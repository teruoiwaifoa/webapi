<?php
  require "common.ph";
  require "cmninstall.ph";
  require "cmndb.ph";
  Header("Access-Control-Allow-Origin: *");
  $tmppackagetop = "c:\\nginx-1.15.5\\html\\share\\tmppackages";
  $createIsoCmd = "c:\\nginx-1.15.5\\html\\install\\cg\\createIso.bat";
  $cmhost = $_GET['cmshost'];
  $cmsport = $_GET['cmsport'];
  $cghost = $_GET['cghost'];
  $cgport = $_GET['cgport'];
  $pname = $_GET['pname'];
  $ntp = $_GET['ntp'];
  $uid = $_GET['uid'];

  $pfolder = file_build_path($packagetop, $pname, "folder");
  $tmpfoldertop = file_build_path($tmppackagetop , $uid);
  
  if (file_exists($tmpfoldertop)) {
      /*既にbatch処理が動いている*/
      $tmpisofilename = file_build_path($tmpfoldertop , "install.iso");
      if (file_exists($tmpisofilename)) {
          /*ダウンロードの準備できた*/
          createResponse();
      }
      else {
          sleep(1);
          headerOfRetry();
      }
  }
  else {
      mkdir($tmpfoldertop);
      $tmpisofilename = "install.iso";
      $tmpisobkfilename = file_build_path($tmpfoldertop,"install.bak");
  
      $tmppackagefolder = file_build_path($tmpfoldertop , "folder");
      mkdir($tmppackagefolder);

      /*最低限のpackageプログラム*/
      xcopy($pfolder,$tmppackagefolder);
  
      /*設定ファイルをコピー*/
      createCgSetFile($tmppackagefolder);

      /*必要な場合はmodule*/
      copyModulesByCg($tmppackagefolder);

      /*isoファイルの作成*/
      createIso($tmpisofilename,$tmpisobkfilename,$tmppackagefolder);
      headerOfRetry();
  }
?>
