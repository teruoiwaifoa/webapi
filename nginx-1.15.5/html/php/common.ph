<?php
/*
POST Methid with JSON parameters.
 */
function curl_post_json($url,$params)
{
	$curl = curl_init();
	curl_setopt_array($curl, array(
	    CURLOPT_URL => $url,
	    CURLOPT_CUSTOMREQUEST => 'POST',
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_POSTFIELDS => json_encode($params),
        CURLOPT_HTTPHEADER => array(
            'Content-Type:application/json',
        ),
    ));
    $response = curl_exec($curl);
    $information = curl_getinfo($curl);
    curl_close($curl);
    return $response;
}

/** 
* Send a POST requst using cURL 
* @param string $url to request 
* @param array $post values to send 
* @param array $options for cURL 
* @return string 
*/ 
function curl_post($url, array $post = NULL, array $options = array()) 
{ 
    $defaults = array( 
        CURLOPT_POST => 1, 
        CURLOPT_HEADER => 0, 
        CURLOPT_URL => $url, 
        CURLOPT_FRESH_CONNECT => 1, 
        CURLOPT_RETURNTRANSFER => 1, 
        CURLOPT_FORBID_REUSE => 1, 
        CURLOPT_TIMEOUT => 4, 
        CURLOPT_POSTFIELDS => http_build_query($post) 
    ); 

    $ch = curl_init(); 
    curl_setopt_array($ch, ($options + $defaults)); 
    if( ! $result = curl_exec($ch)) 
    { 
        trigger_error(curl_error($ch)); 
    } 
    curl_close($ch); 
    return $result; 
} 

/** 
* Send a GET requst using cURL 
* @param string $url to request 
* @param array $get values to send 
* @param array $options for cURL 
* @return string 
*/

function curl_get($url, array $get = NULL, array $options = array()) 
{
    $ch = curl_init(); 
    $defaults = array(
        CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get), 
        CURLOPT_HEADER => 0, 
        CURLOPT_RETURNTRANSFER => TRUE, 
        CURLOPT_TIMEOUT => 4 
    );
    curl_setopt_array($ch, ($options + $defaults)); 
    if( ! $result = curl_exec($ch)) 
    { 
        trigger_error(curl_error($ch)); 
    } 
    curl_close($ch); 
    return $result; 
}

function time2epochmillis($ttime) {
    $year = substr($ttime,0,4);
    $month = substr($ttime,4,2);
    $day = substr($ttime,6,2);
    $hour = substr($ttime,8,2);
    $minute = substr($ttime,10,2);
    return mktime($hour,$minute,0,$month,$day,$year)*1000;
}

function GUID()
{
    if (function_exists('com_create_guid') === true)
    {
        return trim(com_create_guid(), '{}');
    }
    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X',
                   mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535),
                   mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535),
                   mt_rand(0, 65535), mt_rand(0, 65535));
}

function file_build_path(...$segments) {
    return join(DIRECTORY_SEPARATOR, $segments);
}
define("INTERNALHOST","localhost:60000");
?>
