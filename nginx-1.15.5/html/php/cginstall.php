<?php
Header("Access-Control-Allow-Origin: *");
Header("Content-Type: text/html;charset=utf-8");
require "common.ph";
require "cmndb.ph";
require "cmninstall.ph";
$history = getInfoFromHistory();
if ($history == null) {
  $previusdltime = "無";
  $oldcmshost = $cmshost;
  $oldcmsport = $cmsport;
  $oldcgport = $cgport;
  $oldntp = $ntp;
}
else {
  $previusdltime = $history['download'];
  $oldcmshost = $history['cmshost'];
  $oldcmsport = $history['cmsport'];
  $oldcgport = $history['cgport'];
  $oldpackage = $history['packages'];
  $oldntp = $history['ntp'];
}

?>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="/install/cg/main.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script src="/install/cg/main.js"></script>    
    <script>
      $(function() {
          $( "#tabs" ).tabs();
	    });
    </script>
  </head>
  <body>
    <div id="tabs">
      <ul>
	<li><a href="#ini">新規</a></li>
	<li><a href="#re">復旧</a></li>
	<li><a href="#part">部分更新</a></li>               
      </ul>
      <div id="ini">
	CGインストール設定
	<table border="1">
	  <tr><td>CG</td>
	    <td>IP アドレス</td><td id="cghost"><?= $cghost?></td></tr>
	  <tr><td></td>	    
	    <td>ポート番号</td><td id="cgport"><?= $cgport?></td></tr>
	  <tr><td>CMS</td>
	    <td>IP アドレス</td><td id="cmshost"><?= $cmshost?></td></tr>
	  <tr><td></td>
	    <td>ポート番号</td><td id="cmsport"><?= $cmsport?></td></tr>
	  <tr><td>パッケージ</td>
	    <td>名前</td><td><select name='packages' id='selPack1'><?= getHtmlOfPackageList()?> </select></td></tr>	    

	  <tr><td>NTP</td>
	    <td>Server</td><td id="ntp"><?= getNTPConfig() ?></td></tr>
	</table>
	<button id="inidl">Download</button>
	<ul id="inidllist"></ul>
	<div class="loading is-hide">
	  <div class="loading_icon"></div>
	</div>	    
      </div>
      <div id="re">
	インストール設定(過去の履歴設定内容)
	<table border="1">
	  <tr><td>前回DL履歴</td>
	      <td>時刻</td><td><?= $previusdltime ?></td></tr>
	  <tr><td>CG</td>
	    <td>IP アドレス:ポート番号</td><td><?= $cghost . ":" . $oldcgport ?></td></tr>
	  
	  <tr><td>CMS</td>
	    <td>IP アドレス:ポート番号</td><td><?= $oldcmshost . ":" . $oldcmsport ?></td></tr>

	  <!--<tr><td>FE</td>
	    <td>ホスト:ポート</td><td><?= getFeByCg($cghost,$cgport) ?></td></tr> -->
	  
	  <tr><td>NTP</td>
	    <td>Server</td><td id="ntp"><?= $oldntp ?></td></tr>

	  <tr><td>パッケージ</td>
	    <td>名前</td><td><select name='packages' id='selPack2'><?= getHtmlOfPackageList($oldpackage)?> </select></td></tr>

	  <tr><td>モジュールリスト</td>
	    <td></td><td><?= getHtmlOfCtms() ?></td></tr>
	      
	</table>
	<button id="redl">Download</button>
	<ul id="redllist"></ul>
	<div class="loading is-hide">
	  <div class="loading_icon"></div>
	</div>	    
      </div>

      <div id="part">
	部分更新(部分更新であっても、CGのサービスなどは再起動します)
	前回のinstall(<?=  $previusdltime ?>)履歴(から更新されているモジュール一覧
	<table border="1">
	  <tr><td>もじゅる</td><td></td><td></td><td></td><td></td></tr>
	  <tr><td></td><td></td><td></td><td></td><td></td></tr>	  
	</table>
      </div>
    </div>
  </body>
</html>
