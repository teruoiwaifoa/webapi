<?php
  $cmsconf = readCmsConf();
  $dbhost = $cmsconf->{'syb'}->{'database'}->{'hostname'};
  $dbport = $cmsconf->{'syb'}->{'database'}->{'port'};
  $dbname = $cmsconf->{'syb'}->{'database'}->{'databaseName'};
  $dbuser = $cmsconf->{'syb'}->{'database'}->{'user'};
  $dbpasswd = $cmsconf->{'syb'}->{'database'}->{'password'};
  
  $pdohd = "mysql:host=$dbhost;dbname=$dbname;charset=utf8";
//error_log("pdo=" . $pdohd,0);
  $createtblsql = "create table cmsdb.cginstall (
                          download datetime default current_timestamp,
                          cmshost varchar(100),
                          cmsport varchar(100),
                          cghost varchar(100),
                          cgport varchar(100),
                          packages varchar(200),
                          modules varchar(1000),
                          ntp varchar(100)
                   );";
  
function readCmsConf() {
    $conf = "c:\\foa\\cms\\conf\\cms.conf";
    $result = json_decode(file_get_contents($conf));
    return $result;
}
  
function insertHistory() {
    global $pdohd;
    global $dbuser;
    global $dbpasswd;
    global $cmshost;
    global $cmsport;
    global $cghost;
    global $cgport;
    global $pname;
    global $ntp;
    global $uid;
    //$module = getHtmlOfCtms();
    $modules = "";
    $pdo = new PDO($pdohd,$dbuser,$dbpasswd);
    $sql = $pdo->prepare('insert into cginstall(cmshost,cmsport,cghost,cgport,packages,modules,ntp) ' .
                         'values(:cmshost,:cmsport,:cghost,:cgport,:packages,:modules,:ntp)');
    $sql->bindParam(':cmshost',$cmshost,PDO::PARAM_STR);
    $sql->bindParam(':cmsport',$cmsport,PDO::PARAM_STR);    
    $sql->bindParam(':cghost',$cghost,PDO::PARAM_STR);
    $sql->bindParam(':cgport',$cgport,PDO::PARAM_STR);    
    $sql->bindParam(':packages',$pname,PDO::PARAM_STR);
    $sql->bindParam(':modules',$modules,PDO::PARAM_STR);
    $sql->bindParam(':ntp',$ntp,PDO::PARAM_STR);    
    $sql->execute();
}
function getInfoFromHistory() {
    global $pdohd;
    global $dbuser;
    global $dbpasswd;
    global $cghost;
    $pdo = new PDO($pdohd,$dbuser,$dbpasswd);
    $sql = $pdo->prepare("select * from cginstall where cghost='$cghost' order by download desc limit 1");
    $sql->execute();
    if ($sql == null) {
        return null;
    }
    return $sql->fetch(PDO::FETCH_ASSOC);    
}
function getApproveMapping($ctmid) {
    global $pdohd;
    global $dbuser;
    global $dbpasswd;
    global $cghost;
    $pdo = new PDO($pdohd,$dbuser,$dbpasswd);
    $sql = $pdo->prepare("select * from mapping where id='$ctmid'"); // id(string),revision(int),body(json)
    $sql->execute();
    $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    foreach ($result as $row) {
        $cid = $row['id'];
        $rev = $row['revision'];
        $json = json_decode($row['body']);
        $p2Type = $json->{'p2Type'};
        $p2DllName = $json->{'p2DllName'};
        $p2DllVersion = $json->{'dllVersion'};
        $ret = array();
        $ret['cid'] = $cid;
        $ret['rev'] = $rev;
        $ret['p2Type'] = $p2Type;
        $ret['p2DllName'] = $p2DllName;
        $ret['p2DllVersion'] = $p2DllVersion;
        return $ret;
    }
    error_log("Not Detect $ctmid ",0);
    return null;
}
function copyModulesByCg($topfolder) {
    //copyJarModulesByCg($topfolder);
    //copyDllModulesByCg($topfolder);
    global $cghost,$cgport;      
    copyCtmsModulePathByCg($cghost,$cgport,$topfolder);
}
  
function copyJarModulesByCg($topfolder) {
    global $cghost,$cgport;
    $paths = getCtmsJarPathByCg($cghost,$cgport);
    $destfolder = file_build_path($topfolder,"foa","cg","path2J");
    for ($i = 0 ; $i < count($paths) ; $i++) {
        xcopy($paths[$i],$destfolder);
    }
}
function copyDllModulesByCg($topfolder) {
    global $cghost,$cgport;
    $paths = getCtmsDllPathByCg($cghost,$cgport);
    $destfolder = file_build_path($topfolder,"foa","cg","path2");
    foreach ($paths as $key => $val) {
        filecopy($val,file_build_path($destfolder,$key.".dll"));
    }
}

?>
