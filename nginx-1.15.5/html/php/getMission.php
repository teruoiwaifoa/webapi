<?php
require 'common.ph';
$missionid = $_REQUEST['missionid'];
$type = $_REQUEST['type'];
$tstart = time2epochmillis($_REQUEST['start']);
$tend = time2epochmillis($_REQUEST['end']);

if (isset($_REQUEST['limit'])) {
    $limit = $GET['limit'];
}
else {
    $limit = 1000;
}

if (isset($_REQUEST['lang'])) {
    $lang = $_REQUEST['lang'];
}
else {
    $lang = 'ja';
}

if (isset($_REQUEST['nobuly'])) {
    $nobulky = $_REQUEST['nobuly'];
}
else {
    $nobulky = "false";
}

if (isset($_REQUEST['elementinfotype'])) {
    $elementinfotype = $_REQUEST['elementinfotype'];
}
else {
    $elementinfotype = 0;
}
if ($type==0) {
    $url = "http://localhost:60000/cms/rest/mib/mission/pm";
    $params = array(
        'id' => $missionid,
        'start' => $tstart,
        'end' => $tend,
        'lang' => 'ja',
        'testing' => 0,
        'limit' => $limit,
        'noBulky' => false);
}
else if ($type == 1) {
    $url = "http://localhost:60000/grip/rest/search/mission/json/create";
    $guid = GUID();
   
    $params = array(
        'searchId' => $guid,
        'missionId' => $missionid,
        'start' => $tstart,
        'end' => $tend,
        'noBulky' => $nobulky,
        'linkBulky' => 'false',
        'searchByInCondition' => 'auto',
        'skipSameMainKey' => 'true',
        'elementInfoType' => $elementinfotype);
   
    $result = curl_get($url,$params);
    $params = json_decode($result,true);

    $url = "http://localhost:60000/grip/rest/search/mission/json/status/all";
    $params = array(
        'searchId' => $guid
    );
    for (;;) {
        $result = curl_get($url,$params);
        if (preg_match('/Normal/',$result)) {
            break;
        }
    }
    $url = "http://localhost:60000/grip/rest/search/mission/json/result/all";
    $params = array(
        'searchId' => $guid
    );
    $result = curl_get($url,$params);
}
header("Content-Type: application/json; charset=utf-8");
echo curl_get($url,$params);
?>
