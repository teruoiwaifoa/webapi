<?php

$cmshost = "";
$cmsport = "";
$cghost = getRemoteAddr();
$cgport = "30100";
$previusdltime = "";
$packagetop = "c:\\nginx-1.15.5\\html\\share\\packages";


getMyCmsAddr();


/*global  $pakagefolder = $packagetop . "/" . $packagename . "/" . "folder/";*/
/*global  $isofile = $packagetop . "/" . $packagename . "/" . "foa.iso";*/

function getPackageList() {
    global $packagetop;
    $dh = opendir($packagetop);
    $ar = array();
    while (($dirname = readdir($dh)) !== false) {
        if ($dirname == "." || $dirname == "..") {
            continue;
        }
        if (is_dir(file_build_path($packagetop,$dirname))) {
            $ar[] = $dirname;
        }
    }
    closedir($dh);
    rsort($ar);
    return $ar;
}

function getAdminDict() {
    global $cmshost;
    global $cmsport;
    $params = array();
    $adminDictJson =  curl_get("http://$cmshost:$cmsport/cms/rest/adminDict/",$params);
    $adminDict = json_decode($adminDictJson);
    return $adminDict;
}

function getHtmlOfPackageList($packages="") {
    $ret = "";
    $arr = getPackageList();
    $ret = "";
    for ($i = 0 ; $i < count($arr) ; $i++) {
        $pk = $arr[$i];
        if ($packages == "" and $i == 0) {
            $selected = "selected='true'";
        }
        else if ($packages != "" and $packages == $pk) {
            $selected = "selected='true'";
        }
        else {
            $selected = "";
        }
        $ret = $ret . "<option value='$pk' $selected>$pk</option>";
    }
    return $ret;
}

function getHtmlOfFeList() {
    $ret = "";
    $arr = getFeList();
    $ret = "<select name='fes'>";
    for ($i = 0 ; $i < count($arr) ; $i++) {
        $fe = $arr[$i];
        if ($i == 0) {
            $selected = "selected='true'";
        }
        else {
            $selected = "";
        }
        $ret = $ret . "<option value='$fe' $selected>$fe</option>";
    }
    $ret = $ret . "</select>";
    return $ret;
}

function getFeList() {
    global $cmshost;
    global $cmsport;
    $ahsarray = getAdminDict()->{'ahs'};
    $arr = array();
    foreach ($ahsarray as $i => $ahs) {
        $fehost = $ahs->{'fe'};
        $feport = $ahs->{'fePort'};
        $arr[] = $fehost . ":" . $feport;
    }
    return $arr;
}

function getRemoteAddr() {
    return $_SERVER["REMOTE_ADDR"];
}

function getMyCmsAddr() {
    global $cmshost;
    global $cmsport;
    $conffile = "c:/foa/cms/conf/cms.conf";
    $confjson = file_get_contents($conffile);
    $conf = json_decode($confjson);
    $cmshost = $conf->{"myself"}->{"hostname"};
    $cmsport = $conf->{"myself"}->{"port"}; 
    return $cmshost . ":" . $cmsport; 
}

/* find CG INfo from adminDict*/
function getFeByCg($cghost,$cgport) {
    $ahsarray= getAdminDict()->{'ahs'};
    foreach ($ahsarray as $i => $ahs) {
        $fehost = $ahs->{'fe'};
        $feport = $ahs->{'fePort'};
        $cglist = $ahs->{'cgs'};
        foreach ($cglist as $j => $cg) {
            $jcghost = $cg->{'hostname'};
            $jcgport = $cg->{'port'};
            if (($cghost == $jcghost)) {
                return $fehost . ":" . $feport;
            }
        }
    }
    return "";
}

function getHtmlOfCtms() {
    global $cghost;
    global $cgport;
    $arr = getCtmsNameByCg($cghost,$cgport);
    $ret = "";
    if ($arr == null) {
      return $ret;
    }
    return join('<br>',$arr);
    return $ret;
}

/* find CG INfo from adminDict*/
function getCtmsNameByCg($cghost,$cgport) {
    $ahsarray= getAdminDict()->{'ahs'};
    foreach ($ahsarray as $i => $ahs) {
        $fehost = $ahs->{'fe'};
        $feport = $ahs->{'fePort'};
        $cglist = $ahs->{'cgs'};
        foreach ($cglist as $j => $cg) {
            $jcghost = $cg->{'hostname'};
            $jcgport = $cg->{'port'};
            if (($cghost == $jcghost)) {
                $ctms = $cg->{'ctms'};
                $arr = array();
                foreach  ($ctms as $k => $ctm) {
                    $ctmid = $ctm->{'id'};
                    $info = getApproveMapping($ctmid);
                    if ($info == null) {
                        continue;
                    }
                    $ctmname = $ctm->{'displayName'}[0]->{'text'}; // 最初の言語のみ。
                    $ctmtype = $ctm->{'type'};
                    $arr[] = $ctmname;
                }
                return $arr;
            }
        }
        return array();
    }
}
function endsWith($haystack, $needle) {
    return (strlen($haystack) > strlen($needle)) ? (substr($haystack, -strlen($needle)) == $needle) : false;
}

/* find CG INfo from adminDict*/
function copyCtmsModulePathByCg($cghost,$cgport,$dest) {
    $ahsarray= getAdminDict()->{'ahs'};
    foreach ($ahsarray as $i => $ahs) {
        $fehost = $ahs->{'fe'};
        $feport = $ahs->{'fePort'};
        $cglist = $ahs->{'cgs'};
        foreach ($cglist as $j => $cg) {
            $jcghost = $cg->{'hostname'};
            $jcgport = $cg->{'port'};
            if (($cghost == $jcghost)) {
                $ctms = $cg->{'ctms'};
                copyCtmsModulePath($ctms,$dest);
            }
        }
    }
}

/* find CG INfo from adminDict*/
function getCtmsJarPathByCg($cghost,$cgport) {
    $ahsarray= getAdminDict()->{'ahs'};
    foreach ($ahsarray as $i => $ahs) {
        $fehost = $ahs->{'fe'};
        $feport = $ahs->{'fePort'};
        $cglist = $ahs->{'cgs'};
        foreach ($cglist as $j => $cg) {
            $jcghost = $cg->{'hostname'};
            $jcgport = $cg->{'port'};
            if (($cghost == $jcghost)) {
                $ctms = $cg->{'ctms'};
                $patharr = getCtmsJarPath($ctms);
                return $patharr;
            }
        }
        return array();
    }
}

/* find CG INfo from adminDict*/
function getCtmsDllPathByCg($cghost,$cgport) {
    $ahsarray= getAdminDict()->{'ahs'};
    foreach ($ahsarray as $i => $ahs) {
        $fehost = $ahs->{'fe'};
        $feport = $ahs->{'fePort'};
        $cglist = $ahs->{'cgs'};
        foreach ($cglist as $j => $cg) {
            $jcghost = $cg->{'hostname'};
            $jcgport = $cg->{'port'};
            if (($cghost == $jcghost)) {
                $ctms = $cg->{'ctms'};
                $patharr = getCtmsDllPath($ctms);
                return $patharr;
            }
        }
        return array();
    }
}

function getCtmsDllPath($ctms) {
    $patharr = array();
    foreach  ($ctms as $k => $ctm) {
        $ctmid = $ctm->{'id'};
        $info = getApproveMapping($ctmid);
        if ($info == null) { // TODO: Is Correct? iwai
            continue;
        }
        if ($info['p2Type'] != 0) {
            continue; // DLL
        }
        $ctmname = $ctm->{'displayName'}[0]->{'text'};
        $ctmtype = $ctm->{'type'};
        $dllfolder = file_build_path("c:\\foa\\cms\\data\\p2dll");
        $fh = opendir($dllfolder);
        $maxrev = -1;
        while (($file = readdir($fh)) !== false) {
            if (preg_match('/^'.$ctmid.'(_[0-9]+)?\.dll/',$file,$matches) == 1) {
                if (strlen($matches[1]) >= 2) {
                   $rev = (int)substr($matches[1],1);
                   if ($maxrev < $rev) {
                      $maxrev = $rev;
                   }
                }
                else {
                    $maxrev = 0;
                    break;
                }
            }
        }
        closedir($fh);
        
        if ($maxrev == 0) {
            $dllname = $ctmid . ".dll";
            $patharr[$ctmname] = file_build_path("c:\\foa\\cms\\data\\p2dll",$dllname);            
        }
        else if ($maxrev > 0) {
            $dllname = $ctmid . "_" . $maxrev . ".dll";
            $patharr[$ctmname] = file_build_path("c:\\foa\\cms\\data\\p2dll",$dllname);
        }
    }
    return $patharr;
}

function copyCtmsModulePath($ctms,$dest) {
    foreach  ($ctms as $k => $ctm) {
        $ctmid = $ctm->{'id'};
        $info = getApproveMapping($ctmid);
        $ctmid = $info['cid'];
	if (empty($ctmid)) {
	  continue;
	}
        $ctmname = $ctm->{'displayName'}[0]->{'text'};
        $ctmtype = $ctm->{'type'};
        $arr[] = $ctmname;
        if ($info['p2Type'] == 0) {
            $dllflag = true;
            $dllfolder = file_build_path("c:\\foa\\cms\\data\\p2dll",$ctmid . '_' . $info['p2DllVersion'] . ".dll");
            $dstfolder = file_build_path($dest,"foa","cg","path2",$info['p2DllName']);
            filecopy($dllfolder,$dstfolder);
        }
        else if ($info['p2Type'] == 1) {
	    //$revfolder = file_build_path("c:\\foa\\cms\\data\\p2package",$ctmid,(string)$info['p2DllVersion'],$info['p2DllName']);
            $dllfolder = file_build_path("c:\\foa\\cms\\data\\p2dll",$ctmid . '_' . $info['p2DllVersion'] . ".dll");
            $dstfolder = file_build_path($dest,"foa","cg","path2j",$info['p2DllName']);
            filecopy($dllfolder,$dstfolder);
        }
    }
}

function getCtmsJarPath($ctms) {
    $patharr = array();
    foreach  ($ctms as $k => $ctm) {
        $ctmid = $ctm->{'id'};
        $info = getApproveMapping($ctmid);
        if ($info == null) { // TODO: Is Correct? iwai
            continue;
        }
        if ($info['p2Type'] != 1) { // 0:DLL, 1:JAR
            continue; // JAR
        }
        $ctmname = $ctm->{'displayName'}[0]->{'text'};
        $ctmtype = $ctm->{'type'};
        $arr[] = $ctmname;
        $revfolder = file_build_path("c:\\foa\\cms\\data\\p2package",$ctmid);
        $revno = 0;        
        if (is_dir($revfolder)) {
            $fh = opendir($revfolder);
            while (($file = readdir($fh)) !== false) {
                if (preg_match('/[1-9][0-9]*/',$file) == 1) {
                    if (is_dir(file_build_path($revfolder,$file))) {
                        if ((int)$file > $revno) {
                            $revno = (int)$file;
                        }
                    }
                }
	        }
            closedir($fh);            
            //error_log("mod ctmid=" . $ctmid . " rev=" . ((string)$revno),0);
            $jarfolder = file_build_path($revfolder, (string)$revno);
            if (is_dir($jarfolder)) {
                $fh = opendir($jarfolder);
                while (($file = readdir($fh)) !== false) {
                    if (endsWith($file,".jar")) {
                        /* jar file */
                        $patharr[] = file_build_path($jarfolder,$file);
                        break;
                    }
                }
                closedir($fh);
            }
        }
    }
    return $patharr;
}
function getNTPConfig() {
    $cmd = "w32tm /query /status";
    $ph = popen($cmd,"r");
    $contents = stream_get_contents($ph);
    pclose($ph);
    //preg_match('/ソース: +([^ ]+),([^ ]+)/',$contents,$matches);
    preg_match('/: +([^ ]+),([^ ]+)/',$contents,$matches);
    $tsrv = $matches[1];
    $tparam = $matches[2];
    return $tsrv . "," . $tparam    ;
    /*    return "w32tm /config /update /manualpeerlist:\"$tsrv,$tparam\""; */
    /* w32tm /resync */
}

function createCgConf($cghost,$cmshost,$cmsport) {
    $json = "{
    \"cgPort\" : 10100,
    \"logPort\" : 10110,
    \"path3Port\" : 10130, 
    \"devicePort\" : 10120,
    \"hostname\" : \"$cghost\",
    \"p2Listener\" : {
        \"hostname\" : \"localhost\",
        \"port\" : 10200,
        \"connectTimeout\" : 5000,
        \"readTimeout\" : 0
    },
    \"p2Ctrl\" : {
        \"hostname\" : \"localhost\",
        \"port\" : 10210,
        \"connectTimeout\" : 5000,
        \"readTimeout\" : 5000
    },
     \"cms\" : {
        \"hostname\" : \"$cmshost\",
        \"port\" : $cmsport,
        \"connectTimeout\" : 5000,
        \"readTimeout\" : 5000
    },
    \"ctmSendQueueSize\" : 10000,
    \"ctmSendQueueWarnThreshold\" : 5000,
    \"ctmSendQueueErrorThreshold\" : 8000,
        
    \"tagDataReceiveQueueSize\" : 2000,
    \"tagDataReceiveQueueWarnThreshold\" : 1000,
    \"tagDataReceiveQueueErrorThreshold\" : 1600,
    \"p2RequestQueueSize\" : 2000,
    \"feConnectTimeout\" : 5000,
    \"feReadTimeout\" : 10000,
    \"statsPeriod\" : 10000,
    \"maxHenkaTagRate\" : 600000,
    \"usePath3\" : false,
    \"deviceTestFileKeepDays\" : 10,
    \"tagDataQueuePollingInterval\" : 0,
    \"plcSettingDir\" : \"C:/foa/dw/plc/setting\",
    \"connectRetryMaxCount\" : 20,
    \"connectRetryIntervalSeconds\": 3,
    \"ctmjournalPutElemInfo\" : false,
    \"skipDownload\" : false,
    \"skipRunDevices\" : false,
    \"useOpcWrapper\" : false,
    \"opcWrapperAccessParams\" : {
        \"pollingInterval\" : 60000,
        \"alertCount\" : 5
    },
    \"opcWrapperConfigDir\" : \"C:/foa/dw/OPC/config\"
}";
    return $json;

}

function createResponse() {
    /* 履歴を登録*/
    global $uid;
    insertHistory();
    /* ダウンロードの準備*/
    header("Content-Type: application/json; charset=utf-8");
    echo "{ \"dlurl\" : \"/share/tmppackages/$uid/install.iso\"}";
}
function headerOfRetry() {
    /*202 retry*/
    http_response_code(501);
}
function createIso($tmpisofilename,$tmpisobkfilename,$tmppackagefolder)
{
    global $createIsoCmd;
    $cmd = "start /b cmd /c $createIsoCmd $tmpisofilename $tmpisobkfilename $tmppackagefolder > nul";
    $pc  = popen($cmd,"r");
    pclose($pc);

}
function createCgSetFile($folder) {
    global $cghost;
    global $cgport;
    global $cmshost;
    global $cmsport;
    global $ntp;
    $fh =fopen(file_build_path($folder,"foa","cg","conf","cg.conf"),"w");
    $json = createCgConf($cghost,$cmshost,$cmsport);
    fwrite($fh,$json);
    fclose($fh);

    $fh =fopen(file_build_path($folder,"cg_env.bat"),"w");
    fwrite($fh,"SET CGHOST=$cghost" . PHP_EOL);
    fwrite($fh,"SET CGPORT=$cgport" . PHP_EOL);
    fwrite($fh,"SET CMSPORT=$cmsport" . PHP_EOL);
    fwrite($fh,"SET CMSHOST=$cmshost" . PHP_EOL);        
    fwrite($fh,"SET NTP=$ntp" . PHP_EOL);
    fclose($fh);
}
function xcopy($src,$dst) {
    $cmd = "xcopy /S /E /I $src $dst > nul";
    error_log("cmd=" . $cmd,0);
    $pc = popen($cmd,"r");
    pclose($pc);
}
function filecopy($src,$dst) {
    if (!file_exists($src)) {
       error_log("Error: file is not found. " . $src, 0);
       return;
    }
    $cmd = "copy /Y $src $dst > nul";
    error_log("cmd=" . $cmd,0);
    $pc = popen($cmd,"r");
    pclose($pc);
}


?>
