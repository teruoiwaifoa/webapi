$(function () {
    $("#inidl").click(function(){
	var $loading = $(".loading");
	$.ajax({
	    type: 'GET',
	    url:"http://192.192.192.131/php/inidl.php",
	    data: {
		'cghost': getValue("cghost"),
		'cgport': getValue("cgport"),
		'cmshost': getValue("cmshost"),
		'cmsport': getValue("cmsport"),
		'pname': getSelectedPackage("selPack1"),
		'ntp': getValue("ntp"),		
		'uid': getUid()
	    },
	    cache: false,
	    dataType:"json",
	    beforeSend: function() {
		$loading.removeClass("is-hide");
	    },
	    error: function(xhr) {
		if (xhr.status == 501) { 
		    $.ajax(this);  // retry
		}
		return;
	    },
	    loadingHide:function(data){
		$loading.addClass("is-hide");
		//$("#inidl").html("Download"); // change from 'create' to 'download'
		//$("#inidl").click(function() {
		//    anchor = document.createElement("a");
		//    anchor.href = data.dlurl;
		//    anchor.click();
		//});
		location.href="http://192.192.192.131" + data.dlurl;
	    }
	});
    });

    $("#redl").click(function(){
	var $loading = $(".loading");	
	$.ajax({
	    url:"http://192.192.192.131/php/redl.php",
	    dataType:"json",
	    data: {
		'cghost': getValue("cghost"),
		'cgport': getValue("cgport"),
		'cmshost': getValue("cmshost"),
		'cmsport': getValue("cmsport"),
		'pname': getSelectedPackage("selPack2"),
		'ntp': getValue("ntp"),		
		'uid': getUid()	      
	    },
	    cache: false,
	    dataType:"json",
	    beforeSend: function() {
		$loading.removeClass("is-hide");
	    },
	    error: function(xhr) {
		if (xhr.status == 501) { 
		    $.ajax(this);  // retry
		}
		return;
	    },
	    loadingHide:function(data){
		$loading.addClass("is-hide");
		location.href="http://192.192.192.131" + data.dlurl;
	    }
	});
    });

    $(document).on("ajaxSend", function(e,jqXHR,obj){
	var $loading = $(".loading");
	$loading.removeClass("is-hide");
	setTimeout(function(){
	    $.when(jqXHR).done(function(data){
		$loading.addClass("is-hide");
		obj.loadingHide(data);
	    });
	},400);
    });
});

function displayMessage(str) {
  $("#info").html(str);
}    


function getValue(name) {
    return document.getElementById(name).innerText;
}

function getUid() {
    var strong = 1000;
    return Date.now().toString(16) + Math.floor(strong * Math.random()).toString(16);
};

function getSelectedPackage(sid) {
    const sel = document.getElementById(sid);
    const idx = sel.selectedIndex;
    return sel.options[idx].value;
}
