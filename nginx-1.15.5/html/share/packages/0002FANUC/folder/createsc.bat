@powershell -NoProfile -ExecutionPolicy Unrestricted "$s=[scriptblock]::create((gc \"%~f0\"|?{$_.readcount -gt 1})-join\"`n\");&$s" %*&goto:eof

if (Test-Path "C:\Users\Public\Desktop\FOAStudio.lnk") {
  (Get-Item "C:\Users\Public\Desktop\FOAStudio.lnk").Delete()
}
$WsShell = New-Object -ComObject WScript.Shell
$Shortcut = $WsShell.CreateShortcut("C:\Users\Public\Desktop\FOAStudio.lnk")
$Shortcut.TargetPath = "C:\foa\top-menu\FOAStudio.exe"
$Shortcut.IconLocation = "C:\foa\top-menu\FOAStudio.exe"
$Shortcut.Save()
exit 0
