Set-StrictMode -version Latest
Add-Type -AssemblyName System.Windows.Forms;
$text = $args[0];
# https://letspowershell.blogspot.com/2015/06/powershellmessagebox.html 
#ダイアログのタイトル
$caption = "確認";
$buttonsType = "AbortRetryIgnore";
$iconType = "Information";
 
$result = [System.Windows.Forms.MessageBox]::Show($text, $caption, $buttonsType, $iconType);
if ($result -eq "Abort") {
   exit 0;
}
if ($result -eq "Retry")  {
   exit 1;
}
else {
   exit 2;
}
