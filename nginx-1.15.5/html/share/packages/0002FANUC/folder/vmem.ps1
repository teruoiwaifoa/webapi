$rebootflag=$False
$sys = Get-WmiObject -Class Win32_ComputerSystem -EnableAllPrivileges
if ($sys.AutomaticManagedPagefile -eq $False) {

}
else {
  $rebootflag=$True
  $sys.AutomaticManagedPagefile = $False
  $sys.Put()
}
$Total = 0
Get-WmiObject Win32_PhysicalMemory | %{ $Total+=$_.Capacity}
$size = [int]($Total/1MB)
$maxsize = $size*2

$Settings = Get-WmiObject -Class Win32_PageFileSetting | Where-Object { $_.Name -eq 'C:\pagefile.sys' }
if (($Settings.InitialSize -eq $maxsize) -And ($Settings.MaximumSize -eq $maxsize)){

}
else {
  $rebootflag=$True
  $Settings.InitialSize = $maxsize
  $Settings.MaximumSize = $maxsize
  $Settings.Description = "FOA"
  $Settings.Put()
}

if ($rebootflag -eq $True) {
  exit 1
}
else {
  exit 0
}
