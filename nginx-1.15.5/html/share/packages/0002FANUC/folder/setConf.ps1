$CMSHOST = '192.192.192.246'
$FOA_HOMES = 'c:\foa'
$FOA_HOMED = 'c:\\foa'
$FOA_HOMEB = 'c:/foa'

$pathlistS = @("ais/Conf/Ais.conf.template","conf/FoaStudio.conf.template","top-menu/conf/FoaStudio.conf.template")
#$pathlistS = @("ais/Conf/Ais.conf.template")

foreach ($file in $pathlistS) {
   $out = $file -replace ".template",""
   $tmp = $file -replace ".template","-tmp"   
   $(Get-Content $file) -replace '#FOA_HOME#',$FOA_HOMES > $tmp
   $(Get-Content $tmp) -replace '#CMSHOST#',$CMSHOST > $out
}

$pathlistD = @("GripClient/Conf/Grip.conf.template","GripClient/Conf/Grip.conf.template","VideoHome/Conf/RecList.conf.template","VideoHome/Conf/VideoHome.conf.template")
#$pathlistD = @()
foreach ($file in $pathlistD) {
   $out = $file -replace ".template",""
   $tmp = $file -replace ".template","-tmp"
   $(Get-Content $file) -replace "#FOA_HOME#",$FOA_HOMED > $tmp
   $(Get-Content $tmp) -replace "#CMSHOST#",$CMSHOST > $out
}

$pathlistB = @("cms-ui/Conf/CmsUi.conf.template")
