# 古い設定を削除する。
Remove-NetFirewallRule -Group "FOA"

# 新しいルールを追加する。
New-NetFirewallRule `
-Name 'cg2css' -DisplayName 'cg2css' -Description 'CG To CSS' `
-Enabled True -Profile Any -Group "FOA" `
-Direction OutBound -Action Allow -Program Any -LocalAddress Any `
-RemoteAddress $env:OTHERHOST `
-Protocol Any -LocalPort Any -RemotePort Any -LocalUser Any -RemoteUser Any

New-NetFirewallRule `
-Name 'cms2cg' -DisplayName 'cms2cg' -Description 'CMS To CG' `
-Enabled True -Profile Any -Direction InBound -Group "FOA" `
-Action Allow -Program Any -LocalAddress Any `
-RemoteAddress $env:CMSHOST `
-Protocol TCP `
-LocalPort $env:CGPORT,30110 `
-RemotePort Any -LocalUser Any -RemoteUser Any 

# ルールを有効にする。
Set-NetFirewallRule -DisplayName "cg2css" -Enable True
Set-NetFirewallRule -DisplayName "cms2cg" -Enable True
