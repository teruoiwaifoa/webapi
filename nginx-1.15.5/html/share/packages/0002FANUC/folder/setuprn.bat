@powershell -NoProfile -ExecutionPolicy Unrestricted "$s=[scriptblock]::create((gc \"%~f0\"|?{$_.readcount -gt 1})-join\"`n\");&$s" %*&goto:eof

$count = (Get-printer | Select-String -Pattern "Microsoft XPS Document Writer" | Measure-Object).Count
if ($count -eq 0) {
  Add-Printer -Name "Microsoft XPS Document Writer" -DriverName "Microsoft XPS Document Writer v4" -PortName "portprompt:"
}
exit 0
