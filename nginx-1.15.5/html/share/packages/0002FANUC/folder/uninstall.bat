cd /d %~dp0
@ECHO OFF
rem powershell -NoProfile -ExecutionPolicy Unrestricted .\dialogNet.ps1 "ネットワークを利用してインストールしてもよろしいでしょうか(Yes)？ネットワークを利用しないでインストールしますか(No)？インストールを中断しますか(Cancel)?"

call cg_env.bat
powershell -NoProfile -ExecutionPolicy Unrestricted .\dialogVirus.ps1
IF %ERRORLEVEL% neq 0 (
   goto EXIT1
)

REM ▼管理者として実行されているか確認 START
for /f "tokens=1 delims=," %%i in ('whoami /groups /FO CSV /NH') do (
    if "%%~i"=="BUILTIN\Administrators" set ADMIN=yes
    if "%%~i"=="Mandatory Label\High Mandatory Level" set ELEVATED=yes
)

if "%ADMIN%" neq "yes" (
   powershell -NoProfile -ExecutionPolicy Unrestricted .\dialogError.ps1 "このファイルは管理者権限での実行が必要です。 管理者として再実行してください。"
   goto EXIT1
)
if "%ELEVATED%" neq "yes" (
   powershell -NoProfile -ExecutionPolicy Unrestricted .\dialogError.ps1 "このファイルは管理者権限での実行が必要です。 管理者として再実行してください。"
   goto EXIT1
)

REM ▼CG用のサービス動作しているかどうか確認し停止。 CGService,P2LService,OPCWrapper,FoaPlcWrapper
if not exist c:\foa\cg goto NOCGDIR
REM CGフォルダが存在するので、CGのサービスが動作している可能性がある。
set PLCWrapperFlag=0
if not exist c:\foa\dw\plc\WrapperInstallBatch.bat goto NEXT1
sc stop FoaPlcWrapper
set PLCWrapperFlag=1
:NEXT1

set OPCWrapperFLAG=0
if not exist c:\foa\dw\OPC\install_opc_wrapper.bat goto NEXT2
sc stop OPCWrapperService
set OPCWrapperFLAG=1
:NEXT2

set P2LFlag=0
if not exist c:\foa\cg\path2\install_p2.bat goto NEXT3
sc stop P2LService
set P2LFlag=1
:NEXT3

set CGFlag=0
if not exist c:\foa\cg\install_cg.bat goto NEXT4
sc stop CGService
set CGFlag=1
:NEXT4

rem ▼JAVAについては何もしない。

rem ▼ FA Engine インストーラ起動(uninstallできるのか？)
\foa\cg\faengine\FAEngine\Disk1\setup.exe
rem ▲ FA Engine インストーラ起動

rem ▼ファイルのコピー foa\cg,dw -> c:\foa\ へ
if exit c:\foa goto NEXT5
mkdir c:\foa
:NEXT5
xcopy /Y /R /I /H /Q \foa\cg c:\foa
IF %ERRORLEVEL% neq 0 (
   powershell -NoProfile -ExecutionPolicy Unrestricted .\dialogError.ps1 "c:\foaへのコピーが失敗しました。c:\\foaを利用するプログラムを停止し再度install.bat を実行してください。"
   goto EXIT1
)
xcopy /Y /R /I /H /Q \foa\dw c:\foa
IF %ERRORLEVEL% neq 0 (
   powershell -NoProfile -ExecutionPolicy Unrestricted .\dialogError.ps1 "c:\foaへのコピーが失敗しました。c:\\foaを利用するプログラムを停止し再度install.bat を実行してください。"
   goto EXIT1
)
rem ▲ファイルのコピー foa\cg,dw -> c:\foa\ へ


rem ▼ .net 4.7 については何もしない。

rem ▼ firwall を CMS側に開く
if not exist cgfirewall.ps goto NEXT6
powershell -NoProfile -ExecutionPolicy Unrestricted .\cgfirewall.ps1
:NEXT6

rem ▼ サービスの必要であれば登録
if %CGFlag% == 1 goto CGNOADD
c:\foa\cg\install_cg.bat
:CGNOADD

if %P2LFlag% == 1 goto P2LNOADD
c:\foa\cg\path2\install_p2.bat
:P2NOADD

if %PLCFlag% == 1 goto PLCNOADD
c:\foa\dw\plc\WrapperInstallBatch.bat
:PLCNOADD

if %OPCFlag% == 1 goto OPCNOADD
c:\foa\dw\OPC\install_opc_wrapper.bat
:OPCNOADD
rem ▲ サービスの必要であれば登録

rem ▼ 仮想メモリを増やす。(2倍に)
powershell -NoProfile -ExecutionPolicy Unrestricted .\vmem.ps1
rem ▲ 仮想メモリを増やす。(2倍に)

rem ▼ NTPサーバ設定の確認し、設定。
w32tm /query /status | find "ソース:" > nul
if %ERRORLEVEL% equ 1 (
rem NTPサーバの設定。
w32tm /config /update /manualpeerlist:%NTP%
rem NTPサーバの場合、設定後再度service 起動しないと有効にならないらしい。
sc stop W32Time
sc start W32Time
w32tm /resync
)
rem ▲ NTPサーバ設定

rem ▼サービスの起動
sc start CGService
sc start P2LService
sc start FoaPlcWrapper
sc start OPCWrapperService
rem ▲サービスの起動

rem 中断時はここに飛ぶ。
:EXIT1
rem
