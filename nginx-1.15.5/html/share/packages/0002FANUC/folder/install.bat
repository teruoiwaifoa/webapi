cd /d %~dp0
@ECHO OFF
rem powershell -NoProfile -ExecutionPolicy Unrestricted .\dialogNet.ps1 "ネットワークを利用してインストールしてもよろしいでしょうか(Yes)？ネットワークを利用しないでインストールしますか(No)？インストールを中断しますか(Cancel)?"

call cg_env.bat
powershell -NoProfile -ExecutionPolicy Unrestricted .\dialogVirus.ps1
IF %ERRORLEVEL% neq 0 (
   goto EXIT1
)

REM ▼管理者として実行されているか確認 START
echo "管理者権限チェック"
for /f "tokens=1 delims=," %%i in ('whoami /groups /FO CSV /NH') do (
    if "%%~i"=="BUILTIN\Administrators" set ADMIN=yes
    if "%%~i"=="Mandatory Label\High Mandatory Level" set ELEVATED=yes
)

if "%ADMIN%" neq "yes" (
   powershell -NoProfile -ExecutionPolicy Unrestricted .\dialogError.ps1 "このファイルは管理者権限での実行が必要です。 管理者として再実行してください。"
   goto EXIT1
)
if "%ELEVATED%" neq "yes" (
   powershell -NoProfile -ExecutionPolicy Unrestricted .\dialogError.ps1 "このファイルは管理者権限での実行が必要です。 管理者として再実行してください。"
   goto EXIT1
)

echo "サービスの登録チェック"
REM ▼CG用のサービス動作しているかどうか確認し停止。 CGService,P2LService,OPCWrapper,FoaPlcWrapper

sc query FoaPlcWrapper | find "STATE" > nul
IF %ERRORLEVEL% equ 1 (
  echo "check plc"
) ELSE (
  sc stop FoaPlcWrapper
  echo "uninstall PLC Wrapper"  
  call .\foa\dw\plc\WrapperUninstallBatch.bat
)  

sc query OPCWrapperService | find "STATE" > nul
IF %ERRORLEVEL% equ 1 (
  echo "check opc"
) ELSE (
  sc stop OPCWrapperService
  echo "uninstall OPC Wrapper"    
  call .\foa\dw\OPC\uninstall_opc_wrapper.bat
)

sc query P2LService | find "STATE" > nul
if %ERRORLEVEL% equ 1 (
  echo "check p2l"
) ELSE (
  sc stop P2LService
  echo "uninstall P2L"      
  call .\foa\cg\path2\uninstall_p2.bat
)

sc query CGService | find "STATE" > nul
if %ERRORLEVEL% equ 1 (
  echo "check cg"
) ELSE (
  sc stop CGService
  echo "uninstall CG"        
  call .\foa\cg\uninstall_cg.bat  
)

rem ▼JAVAのインストールの確認。
echo "JAVAのチェック"
IF "%JAVA_HOME%" == "" (
  call .\jdk-8u202-windows-x64.exe
  wmic environment create name="JAVA_HOME", username="<SYSTEM>", variablevalue="C:\Program Files\Java\jdk1.8.0_202"
  set JAVA_HOME="C:\Program Files\Java\jdk1.8.0_202"  
) ELSE (
  ECHO JAVAは既にインストールされていると判断しました
)

rem ▼ファイルのコピー foa\cg,dw -> c:\foa\ へ
echo "ファイルコピー"
if exist c:\foa goto NEXT5
mkdir c:\foa
:NEXT5
xcopy /S /E /Y /R /I /H /Q .\foa\* c:\foa
IF %ERRORLEVEL%  equ 0 (
   echo "Copy finished"
) ELSE (
   powershell -NoProfile -ExecutionPolicy Unrestricted .\dialogError.ps1 "c:\foaへのコピーが失敗しました。c:\\foaを利用するプログラムを停止し再度install.bat を実行してください。"
   goto EXIT1
)
rem ▲ファイルのコピー foa\cg,dw -> c:\foa\ へ

rem ▼ FA Engine インストーラ起動
powershell -NoProfile -ExecutionPolicy Unrestricted .\chkfa.ps1 > nul
if %ERRORLEVEL% eq 1 (
  call .\FAEngine\Disk1\setup.exe
)
rem ▲ FA Engine インストーラ起動

rem ▼ .net 4.7 がインストールされていることを確認。 (Win10PRO以降は問題なし)
Dism /Online /Enable-Feature:NetFx4-AdvSrvs


rem ▼ firewall を CMS側に開く
if defined FEHOST (
  set OTHERHOST=%CMSHOST%,%FEHOST%
) ELSE (
  set OTHERHOST=%CMSHOST%
) 

if not exist .\cgfirewall.ps1 goto NEXT6
powershell -NoProfile -ExecutionPolicy Unrestricted .\cgfirewall.ps1
:NEXT6

rem ▼ サービスの必要であれば登録
echo "CG Service を追加"
call c:\foa\cg\install_cg.bat
echo "P2LServiceを追加"
call c:\foa\cg\path2\install_p2.bat
echo "PLC Wrapper Serviceを追加"
call c:\foa\dw\plc\WrapperInstallBatch.bat
echo "OPC Wrapper Serviceを追加"
call c:\foa\dw\OPC\install_opc_wrapper.bat
rem ▲ サービスの必要であれば登録

rem ▼ 仮想メモリを増やす。(2倍に)
echo "仮想メモリの設定"
powershell -NoProfile -ExecutionPolicy Unrestricted .\vmem.ps1
rem wmic pagefileset delete
rem wmic pagefileset create Description="FOA",InitialSize=4096,MaximumSize=8192,Name="c:\pagefile.sys"
rem ▲ 仮想メモリを増やす。(2倍に)

rem ▼ NTPサーバ設定の確認し、設定。
sc query w32time | find "RUNNING" > nul
if %ERRORLEVEL% equ 1 (
  rem NTPサーバ起動させる。
  sc start w32time
)

w32tm /query /status | find "ソース: Local CMOS Clock" > nul
if %ERRORLEVEL% equ 0 (
rem NTPサーバの設定。
echo "NTPサーバ設定"
w32tm /config /update /manualpeerlist:%NTP%
rem NTPサーバの場合、設定後再度service 起動しないと有効にならないらしい。
sc stop W32Time
sc config W32Time start=auto
sc start W32Time
w32tm /resync
)
rem ▲ NTPサーバ設定

echo "サービスを遅延実行時に起動に設定"
sc config CGService start=delayed-auto
sc config P2LService start=delayed-auto
sc config FoaPlcWrapper start=delayed-auto
sc config OPCWrapperService start=delayed-auto
rem ▼サービスの起動
echo "Serviceを起動します。"
sc start CGService
sc start P2LService
sc start FoaPlcWrapper
sc start OPCWrapperService
rem ▲サービスの起動

rem 中断時はここに飛ぶ。
:EXIT1
rem

powershell -NoProfile -ExecutionPolicy Unrestricted .\dialogWarn.ps1 "OSを再起動してください。"

echo "終了しました。"
