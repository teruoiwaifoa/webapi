sc stop CGService
sc stop P2LService
sc stop FoaPlcWrapper
sc stop OPCWrapperService

call c:\foa\cg\uninstall_cg.bat
call c:\foa\cg\path2\uninstall_p2.bat
call c:\foa\dw\plc\WrapperUninstallBatch.bat
call c:\foa\dw\opc\uninstall_opc_wrapper.bat

sc query CGService
sc query P2LService
sc query FoaPlcWrapper
sc query OPCWrapperService
