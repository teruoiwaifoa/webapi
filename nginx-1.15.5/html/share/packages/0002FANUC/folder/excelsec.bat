@echo off
reg query "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Excel\Security"
IF NOT ERRORLEVEL 0 (
  echo Excel2016 is not installed.
) ELSE (
  echo Excel2016 security policies were changed.
  reg add "HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Excel\Security" /v "AccessVBOM" /t REG_DWORD /d "1" /f
  reg add "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\16.0\Excel\Security" /v "VBAWarnings" /t REG_DWORD /d "1" /f
)

reg query "HKEY_CURRENT_USER\Software\Microsoft\Office\15.0\Excel\Security"
IF NOT ERRORLEVEL 0 (
  echo Excel2013 is not installed.
) ELSE (
  echo Excel2013 security policies were changed.
  reg add "HKEY_CURRENT_USER\Software\Microsoft\Office\15.0\Excel\Security" /v "AccessVBOM" /t REG_DWORD /d "1" /f
  reg add "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\15.0\Excel\Security" /v "VBAWarnings" /t REG_DWORD /d "1" /f  
)
