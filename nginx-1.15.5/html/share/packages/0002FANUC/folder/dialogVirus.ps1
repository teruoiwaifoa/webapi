Set-StrictMode -version Latest

#.NET Frameworkのダイアログ関連オブジェクトの取り込み
Add-Type -AssemblyName System.Windows.Forms;
 
#ダイアログに表示する説明
$text = "ウィルスバスターなどのウィルスチェックソフトウェアを停止してください。よろしいでしょうか？";
 
#ダイアログのタイトル
$caption = "チェック";
 
#ボタンの形状
$buttonsType = "OKCancel";
 
#アイコンの種類
$iconType = "Question";
 
$result = [System.Windows.Forms.MessageBox]::Show($text, $caption, $buttonsType, $iconType);
if ($result -eq "OK") {
  exit 0;
}
else {
  exit 1;
}
