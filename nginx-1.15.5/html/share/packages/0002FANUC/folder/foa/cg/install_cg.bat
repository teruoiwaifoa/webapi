rem set CG_HOME=%~dp0
set CG_HOME=c:\foa\cg\
set CG_LIB=%CG_HOME%lib

%CG_HOME%CGService.exe //IS//CGService ^
--DisplayName="CGService" ^
--Description="CGService" ^
--Install=%CG_HOME%CGService.exe ^
--Classpath=%CG_LIB%\cg.jar ^
--Jvm="%JAVA_HOME%\jre\bin\server\jvm.dll" ^
--LogPath=%CG_HOME%logs ^
--StdOutput=auto ^
--StdError=auto ^
--StartClass=com.foa.cg.CGService ^
--StartMethod=startService ^
--StartMode=jvm ^
--StartPath=%CG_HOME% ^
--JvmMs=64 ^
--JvmMx=1280 ^
--StopClass=com.foa.cg.CGService ^
--StopMethod=stopService ^
--StopMode=jvm ^
--StopPath=%CG_HOME%
