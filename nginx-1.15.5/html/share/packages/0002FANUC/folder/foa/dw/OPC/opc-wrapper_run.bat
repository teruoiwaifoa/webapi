@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  opc-wrapper startup script for Windows
@rem
@rem ##########################################################################

title opc-wrapper


@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

@rem Add default JVM options here. You can also use JAVA_OPTS and OPC_WRAPPER_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS=
set JAVA_OPTS="-Dcom.foa.opc.wrapper.home.dir=c:/foa/dw/OPC -Dcom.foa.cg.home.dir=c:/foa/cg"

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto init

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:init
@rem Get command-line arguments, handling Windowz variants

if not "%OS%" == "Windows_NT" goto win9xME_args
if "%@eval[2+2]" == "4" goto 4NT_args

:win9xME_args
@rem Slurp the command line arguments.
set CMD_LINE_ARGS=
set _SKIP=2

:win9xME_args_slurp
if "x%~1" == "x" goto execute

set CMD_LINE_ARGS=%*
goto execute

:4NT_args
@rem Get arguments from the 4NT Shell from JP Software
set CMD_LINE_ARGS=%$

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\opc-wrapper.jar;%APP_HOME%\lib\opc-ua-stack-1.3.346-197.jar;%APP_HOME%\lib\prosys-opc-ua-java-sdk-client-3.1.8-580.jar;%APP_HOME%\lib\slf4j-api-1.7.12.jar;%APP_HOME%\lib\foa-core.jar;%APP_HOME%\lib\cg.jar;%APP_HOME%\lib\logback-core-1.1.3.jar;%APP_HOME%\lib\logback-classic-1.1.3.jar;%APP_HOME%\lib\log4j-1.2.17.jar;%APP_HOME%\lib\simple-5.1.6.jar;%APP_HOME%\lib\jsr311-api-1.1.1.jar;%APP_HOME%\lib\jersey-server-1.19.jar;%APP_HOME%\lib\jersey-core-1.19.jar;%APP_HOME%\lib\jersey-servlet-1.19.jar;%APP_HOME%\lib\jersey-client-1.19.jar;%APP_HOME%\lib\jersey-multipart-1.19.jar;%APP_HOME%\lib\jackson-databind-2.3.5.jar;%APP_HOME%\lib\jackson-annotations-2.3.5.jar;%APP_HOME%\lib\jackson-core-2.3.5.jar;%APP_HOME%\lib\commons-codec-1.10.jar;%APP_HOME%\lib\commons-collections-3.2.2.jar;%APP_HOME%\lib\commons-daemon-1.0.14.jar;%APP_HOME%\lib\httpclient-4.5.2.jar;%APP_HOME%\lib\httpcore-4.4.4.jar;%APP_HOME%\lib\httpcore-nio-4.4.4.jar;%APP_HOME%\lib\opencsv-3.5.jar;%APP_HOME%\lib\core-1.52.0.0.jar;%APP_HOME%\lib\pkix-1.52.0.0.jar;%APP_HOME%\lib\prov-1.52.0.0.jar;%APP_HOME%\lib\bcpkix-jdk15on-1.54.jar;%APP_HOME%\lib\bcprov-jdk15on-1.54.jar;%APP_HOME%\lib\mongo-java-driver-3.2.2.jar;%APP_HOME%\lib\libthrift-0.9.3.jar;%APP_HOME%\lib\commons-csv-1.4.jar;%APP_HOME%\lib\mimepull-1.9.3.jar;%APP_HOME%\lib\guava-19.0.jar;%APP_HOME%\lib\commons-logging-1.2.jar;%APP_HOME%\lib\commons-lang3-3.3.2.jar

@rem Execute opc-wrapper
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %OPC_WRAPPER_OPTS%  -classpath "%CLASSPATH%" com.foa.opc.wrapper.OpcWrapperMain %CMD_LINE_ARGS%

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable OPC_WRAPPER_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%OPC_WRAPPER_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
