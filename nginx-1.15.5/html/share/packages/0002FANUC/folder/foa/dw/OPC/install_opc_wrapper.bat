set OPC_WRAPPER_HOME=c:\foa\dw\OPC\
set OPC_WRAPPER_LIB=%OPC_WRAPPER_HOME%lib

%OPC_WRAPPER_HOME%OpcWrapperService.exe //IS//OpcWrapperService ^
--DisplayName="OpcWrapperService" ^
--Description="OpcWrapperService" ^
--Install=%OPC_WRAPPER_HOME%OpcWrapperService.exe ^
--Classpath=%OPC_WRAPPER_LIB%\opc-wrapper.jar ^
--Jvm="%JAVA_HOME%\jre\bin\server\jvm.dll" ^
--LogPath=%OPC_WRAPPER_HOME%logs ^
--StdOutput=auto ^
--StdError=auto ^
--StartClass=com.foa.opc.wrapper.OpcWrapperService ^
--StartMethod=startService ^
--StartMode=jvm ^
--StartPath=%OPC_WRAPPER_HOME% ^
--JvmMs=64 ^
--JvmMx=1280 ^
--StopClass=com.foa.opc.wrapper.OpcWrapperService ^
--StopMethod=stopService ^
--StopMode=jvm ^
--StopPath=%OPC_WRAPPER_HOME% ^
--JvmOptions=-Dcom.foa.opc.wrapper.home.dir=c:/foa/dw/OPC ^
++JvmOptions=-Dcom.foa.cg.home.dir=c:/foa/cg

