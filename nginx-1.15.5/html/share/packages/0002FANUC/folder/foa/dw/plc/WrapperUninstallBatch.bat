@echo off
REM サービスプログラム インストールバッチファイル 
REM 管理者権限で実行してください
REM ローカルコンピュータをターゲットとしているので、Servernameは指定しないで実行します
REM 注意：パラメータ名の=のあとは半角スペースを要する
REM obj:サービスが実行されるアカウント名(デフォルト = LocalSystem)

REM サービスの設定 ************************************************************
SET SRVICE_NAME=FoaPlcWrapper
SET BIN_PATH=C:\foa\dw\plc\FoaPlcWrapper.exe
SET DISPLAYNAME="FoaPlcWrapper"
SET OBJ=username
SET PASSWORD=hogehogehoge

REM サービスの登録 ************************************************************ 
REM sc create %SRVICE_NAME% binpath= %BIN_PATH% start= demand displayname= %DISPLAYNAME% 
REM  obj= %OBJ% password= %PASSWORD%

REM サービスの削除 ************************************************************
sc delete %SRVICE_NAME%
