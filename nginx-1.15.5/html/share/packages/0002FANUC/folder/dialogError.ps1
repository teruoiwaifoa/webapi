Set-StrictMode -version Latest

#.NET Frameworkのダイアログ関連オブジェクトの取り込み
Add-Type -AssemblyName System.Windows.Forms;
 
#ダイアログに表示する説明
$text = $args[0];
 
#ダイアログのタイトル
$caption = "チェック";
 
#ボタンの形状
$buttonsType = "OK";
 
#アイコンの種類
$iconType = "Error";
 
$result = [System.Windows.Forms.MessageBox]::Show($text, $caption, $buttonsType, $iconType);
$result;

