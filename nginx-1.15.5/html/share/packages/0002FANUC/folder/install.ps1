# 0. Administrator Check

# 1. JAVA HOME kakunin
\jdk-8u202-windows-x64.exe
#2. .net4.6.2 kakunin

#3. firewall
\cgfirewall.ps1

#4. CG service regist
\foa\cg\install_cg.bat
Start-Service -Name CGService

#5. vitual memory
$Settings = Get-WmiObject -Class Win32_PageFileSetting | Where-Object { $_.Name -eq 'C:\pagefile.sys' }
$Settings.InitialSize = 8192
$Settings.MaximumSize = 4096
$Settings.Put()

#6. FA Engine Install
\foa\cg\faengine\FAEngine\Disk1\setup.exe

#7. P2L service install
cd \foa\cg\path2
\foa\cg\path2\install_p2.bat
Start-Service -name P2LService

#8. PlcWrapper install
cd \foa\dw\plc
\foa\dw\plc\WrapperInstallBatch.bat
Start-Service -name FoaPlcWrapper

#9. OpcWrapper install

